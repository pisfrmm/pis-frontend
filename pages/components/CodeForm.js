import axios from 'axios';
import {useState} from 'react'
import Editor from "@monaco-editor/react";
import Monaco from './Monaco';

const GETFORM_FORM_ENDPOINT = "http://127.0.0.1:9080/response/postbody";
function CodeForm() {
    const [formStatus, setFormStatus] = useState(false);
    const [query, setQuery] = useState({
        name: "",
        email: "",
        language: "js",
        file: "",
        code: ""
    });
    const [codeOutput, setCodeOutput] = useState("blank output")

    const handleFileChange = () => (e) => {
        setQuery((prevState) => ({
            ...prevState,
            file: e.target.files[0]
        }));
    };
    const handleChange = () => (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setQuery((prevState) => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleCodeChange = (newValue) => {
        setQuery((prevState) => ({
            ...prevState,
            code: newValue
        }));
    };
    
    const handleSubmit = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.entries(query).forEach(([key, value]) => {
            formData.append(key, value);
        });

        var object = {};
        formData.forEach((value, key) => object[key] = value);
        
        const res = await fetch(GETFORM_FORM_ENDPOINT, {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify(object)
        })
        const data = await res.text()
        setCodeOutput(codeOutput=data);
        document.getElementById('myTextarea').value = codeOutput;
        console.log(codeOutput)

        console.log(JSON.stringify(object));

    };

    

    return (
        <div class="container-md">
        <h2>Upload File with your solution</h2>
                    <form
                        acceptCharset="UTF-8"
                        method="POST"
                        enctype="multipart/form-data"
                        id="ajaxForm"
                        onSubmit={handleSubmit}
                    >
                        <div className="form-group mb-2">
                            <label for="exampleInputEmail1">Email address</label>
                            <input
                                type="email"
                                className="form-control"
                                id="exampleInputEmail1"
                                aria-describedby="emailHelp"
                                placeholder="Enter email"
                                required
                                name="email"
                                value={query.email}
                                onChange={handleChange()}
                            />
                        </div>
                        <div className="form-group mb-2">
                            <label for="exampleInputName">Name</label>
                            <input
                                type="text"
                                className="form-control"
                                id="exampleInputName"
                                placeholder="Enter your name"
                                required
                                name="name"
                                value={query.name}
                                onChange={handleChange()}
                            />
                        </div>
                        <div className="form-group">
                            <label for="exampleFormControlSelect1">Programming language of your's </label>
                            <select
                                className="form-control"
                                id="exampleFormControlSelect1"
                                required
                                name="language"
                                value={query.language}
                                onChange={handleChange()}
                            >
                                <option>js</option>
                                <option>java</option>
                                <option>python</option>
                            </select>
                        </div>
                        {/* <hr/> */}
                        {/* <div className="form-group mt-3">
                            <label className="mr-2">Upload your code: </label><br/>
                            <input name="file" type="file" onChange={handleFileChange}/>
                        </div> */}
                        <hr/>
                        <div >
                            <label className="mr-2">Write your code: </label><br/>
                            <Monaco parentCallback = {handleCodeChange} lang={query.language}></Monaco>
                        </div>
                        <hr/>
                        {formStatus ? (
                            <div className="text-success mb-2">
                                Your message has been sent.
                            </div>
                        ) : (
                            ""
                        )}
                        <button type="submit" className="btn btn-primary" data-testid="submit">Submit</button>
                        <textarea id="myTextarea" style={{
  "width": "100%",
  "height": "150px",
  "padding": "12px 20px",
  "box-sizing": "border-box",
  "border": "2px solid #ccc",
  "border-radius": "4px",
  "background-color": "#f8f8f8",
  "font-size": "16px",
  "resize": "none"
}}>Code Output</textarea>
                    </form>
                </div>
            );
    }
export default CodeForm