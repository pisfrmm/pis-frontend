import {useState} from 'react';

const GET_RESULT_ENDPOINT = "http://127.0.0.1:9080/response/result";

function Result() {
    const [resultStatus, setResultStatus] = useState("");
    const [result, setResult] = useState("");

    const getServerSideProps = async (context) => {
        const res = await fetch(GET_RESULT_ENDPOINT)
        const data = await res.json()
    
        if (!data) {
        return setResult(JSON.stringify(5));
        }
    
        setResult(data)
    }
    return (
        <>
        <div align="center">
            <button  onClick={getServerSideProps}>RUN</button>
        </div>
          <div align="left" text={result}>
            <br/>
            {result}
            <hr/>
            </div>
        </>
    )
}
export default Result