import React, { useRef } from "react";
import ReactDOM from "react-dom";

import Editor from "@monaco-editor/react";

function Monaco({parentCallback, lang}) {
    const editorRef = useRef(null);
  
    function handleEditorDidMount(editor, monaco) {
      editorRef.current = editor; 
    }
    
    function handleCodeValue() {
        editorRef.current.defaultLanguage=lang
        const value = editorRef.current.getValue();
        parentCallback(value)
        // console.log(value)
    }
  
    return (
     <>
       {/* <button onClick={}>Show value</button> */}

       <Editor
         height="20vh"
         defaultLanguage="javascript"
         defaultValue="// some comment"
         onMount={handleEditorDidMount}
         onChange={handleCodeValue}
       />
     </>
    );
  }
  
export default Monaco