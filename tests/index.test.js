import CodeForm from "../pages/components/CodeForm";
import "@testing-library/jest-dom";
import { fireEvent, render, screen } from "@testing-library/react";

describe("CodeForm", () => {
    it("renders a codeForm", () => {
      render(<CodeForm />);
      // check if all components are rendered
      expect(screen.getByTestId("submit")).toBeInTheDocument();
      
    });
  });